# Static finite state machine

Contains base traits used by the [sfsm](https://gitlab.com/sfsm/sfsm) library. Head over there for a more detailed description of the project.